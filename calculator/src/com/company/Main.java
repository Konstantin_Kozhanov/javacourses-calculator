package com.company;

import java.util.Scanner;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Добро пожаловать, начнем вычиления:");
        System.out.println("Введите одну арифметическую опреацию и поставьте знак равно, после чего нажмите Enter");
        String com = in.nextLine();
        int dl = com.length();
        com = com.substring(0, dl-1);
        char z;
        double a = 0;
        z='+';
        if (com.indexOf(z)>0) {
            int nz = com.indexOf(z);
            a = Integer.parseInt(com.substring(0, nz)) + Integer.parseInt(com.substring(nz+1));
        }
        z='-';
        if (com.indexOf(z)>0) {
            int nz = com.indexOf(z);
            a = Integer.parseInt(com.substring(0, nz)) - Integer.parseInt(com.substring(nz+1));
        }
        z='*';
        if (com.indexOf(z)>0) {
            int nz = com.indexOf(z);
            a = Integer.parseInt(com.substring(0, nz)) * Integer.parseInt(com.substring(nz+1));
        }
        z='/';
        if (com.indexOf(z)>0) {
            int nz = com.indexOf(z);
            a = Integer.parseInt(com.substring(0, nz)) / Integer.parseInt(com.substring(nz+1));
        }
        System.out.println(a);
        com = Double.toString(a);

        try  {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) {
        }

        JFrame frm = new JFrame("РЕЗУЛЬТАТ ВЫЧИСЛЕНИЙ");
        frm.setSize(300, 200);
        Container c = frm.getContentPane();
        c.add(new JLabel(com));

        WindowListener wndCloser = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        };
        frm.addWindowListener(wndCloser);

        frm.setVisible(true);

    }
}
