package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculatorEngine implements ActionListener {
    Calculator parent;
    char selectedAction = ' ';
    double currentResult = 0;

    CalculatorEngine(Calculator parent) {
        this.parent = parent;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clickedButton = (JButton) e.getSource();
        String dispFieldText = parent.displayField.getText();
        double displayValue = 0;
        if (!"".equals(dispFieldText) & selectedAction != '#' & selectedAction != '^' & selectedAction != 'E' & selectedAction != 'L' & selectedAction != '!') {
            try {
                displayValue = Double.parseDouble(dispFieldText);
            } catch (NumberFormatException exceptionObject){
                displayValue = 0;
                System.out.println("NumberFormatException");
            }
        }
        Object src = e.getSource();
        if (src == parent.buttonPlus) {
            selectedAction = '+';
            currentResult = displayValue;
            parent.displayField.setText("");
        } else if (src == parent.buttonMinus) {
            selectedAction = '-';
            currentResult = displayValue;
            parent.displayField.setText("");
        } else if (src == parent.buttonDivide) {
            selectedAction = '/';
            currentResult = displayValue;
            parent.displayField.setText("");
        } else if (src == parent.buttonMultiply) {
            selectedAction = '*';
            currentResult = displayValue;
            parent.displayField.setText("");
        } else if (src == parent.buttonLn) {
            selectedAction = 'L';
            currentResult = displayValue;
            parent.displayField.setText("ln(" + currentResult + ")");
        } else if (src == parent.buttonExp) {
            selectedAction = 'E';
            currentResult = displayValue;
            parent.displayField.setText("exp^(" + currentResult + ")");
        } else if (src == parent.buttonF) {
            selectedAction = '!';
            currentResult = displayValue;
            parent.displayField.setText((int)currentResult + "!");
        } else if (src == parent.buttonSqrt) {
            selectedAction = '#';
            currentResult = displayValue;
            parent.displayField.setText("sqrt(" + currentResult + ")");
        } else if (src == parent.buttonSqr) {
            selectedAction = '^';
            currentResult = displayValue;
            parent.displayField.setText("(" + currentResult + ")^2");
        } else if (src == parent.buttonZnak) {
            currentResult = -displayValue;
            parent.displayField.setText("" + currentResult);
        } else if (src == parent.buttonClear) {
            currentResult = 0;
            parent.displayField.setText("");
        } else if (src == parent.buttonDelete) {
            currentResult = Math.floor(displayValue / 10);
            parent.displayField.setText("" + currentResult);
        } else if (src == parent.buttonEqual) {
            if (selectedAction == '+') {
                currentResult += displayValue;
                parent.displayField.setText("" + currentResult);
            } else if (selectedAction == '-') {
                currentResult -= displayValue;
                parent.displayField.setText("" + currentResult);
            } else if (selectedAction == '/') {
                currentResult /= displayValue;
                parent.displayField.setText("" + currentResult);
            } else if (selectedAction == '*') {
                currentResult *= displayValue;
                parent.displayField.setText("" + currentResult);
            } else if (selectedAction == 'L') {
                selectedAction = '=';
                currentResult = Math.log(currentResult);
                parent.displayField.setText("" + currentResult);
            } else if (selectedAction == 'E') {
                selectedAction = '=';
                currentResult = Math.pow(Math.E, currentResult);
                parent.displayField.setText("" + currentResult);
            } else if (selectedAction == '!') {
                selectedAction = '=';
                int fak = 1;
                int i = (int)currentResult;
                while (i != 0){
                    fak = fak * i;
                    i--;
                }
                parent.displayField.setText("" + fak);
            } else if (selectedAction == '#') {
                selectedAction = '=';
                currentResult = Math.sqrt(currentResult);
                parent.displayField.setText("" + currentResult);
            } else if (selectedAction == '^') {
                selectedAction = '=';
                currentResult = currentResult * currentResult;
                parent.displayField.setText("" + currentResult);
            }
        } else {
            String clickedButtonLabel = clickedButton.getText();
            parent.displayField.setText(dispFieldText + clickedButtonLabel);
        }
    }
}