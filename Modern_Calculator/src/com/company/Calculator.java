package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class Calculator {

    JPanel windowContent;
    JTextField displayField;
    JButton button0;
    JButton button1;
    JButton button2;
    JButton button3;
    JButton button4;
    JButton button5;
    JButton button6;
    JButton button7;
    JButton button8;
    JButton button9;
    JButton buttonPoint;
    JButton buttonEqual;
    JButton buttonPlus;
    JButton buttonMinus;
    JButton buttonDivide;
    JButton buttonMultiply;
    JButton buttonClear;
    JButton buttonSqrt;
    JButton buttonSqr;
    JButton buttonDelete;
    JButton buttonZnak;
    JButton buttonLn;
    JButton buttonExp;
    JButton buttonF;
    JPanel p1;

    Calculator(){
        windowContent = new JPanel();

        BorderLayout b1 = new BorderLayout();
        windowContent.setLayout(b1);

        displayField = new JTextField(30);
        windowContent.add("North", displayField);

        button0 = new JButton("0");
        button1 = new JButton("1");
        button2 = new JButton("2");
        button3 = new JButton("3");
        button4 = new JButton("4");
        button5 = new JButton("5");
        button6 = new JButton("6");
        button7 = new JButton("7");
        button8 = new JButton("8");
        button9 = new JButton("9");
        buttonPoint = new JButton(".");
        buttonEqual = new JButton("=");
        buttonPlus = new JButton("+");
        buttonMinus = new JButton("-");
        buttonDivide = new JButton("/");
        buttonMultiply = new JButton("*");
        buttonClear = new JButton("C");
        buttonSqrt = new JButton("SQRT");
        buttonSqr = new JButton("x^2");
        buttonDelete = new JButton("DEL");
        buttonZnak = new JButton("+/-");
        buttonLn = new JButton("LnX");
        buttonExp = new JButton("exp^X");
        buttonF = new JButton("!");

        Font BigFont = new Font("TimesRoman", Font.BOLD, 20);

        button0.setFont(BigFont);
        button1.setFont(BigFont);
        button2.setFont(BigFont);
        button3.setFont(BigFont);
        button4.setFont(BigFont);
        button5.setFont(BigFont);
        button6.setFont(BigFont);
        button7.setFont(BigFont);
        button8.setFont(BigFont);
        button9.setFont(BigFont);
        buttonPoint.setFont(BigFont);
        buttonEqual.setFont(BigFont);
        buttonPlus.setFont(BigFont);
        buttonMinus.setFont(BigFont);
        buttonDivide.setFont(BigFont);
        buttonMultiply.setFont(BigFont);
        buttonClear.setFont(BigFont);
        buttonSqrt.setFont(BigFont);
        buttonSqr.setFont(BigFont);
        buttonDelete.setFont(BigFont);
        buttonZnak.setFont(BigFont);
        buttonLn.setFont(BigFont);
        buttonExp.setFont(BigFont);
        buttonF.setFont(BigFont);
        displayField.setFont(BigFont);

        buttonEqual.setBackground(Color.ORANGE);
        buttonClear.setBackground(Color.ORANGE);
        buttonPlus.setForeground(Color.ORANGE);
        buttonMinus.setForeground(Color.ORANGE);
        buttonDivide.setForeground(Color.ORANGE);
        buttonMultiply.setForeground(Color.ORANGE);
        buttonPlus.setBackground(Color.BLACK);
        buttonMinus.setBackground(Color.BLACK);
        buttonDivide.setBackground(Color.BLACK);
        buttonMultiply.setBackground(Color.BLACK);

        p1 = new JPanel();
        GridLayout g1 = new GridLayout(4, 3);
        p1.setLayout(g1);

        p1.add(button0);
        p1.add(button1);
        p1.add(button2);
        p1.add(button3);
        p1.add(button4);
        p1.add(button5);
        p1.add(button6);
        p1.add(button7);
        p1.add(button8);
        p1.add(button9);
        p1.add(buttonPoint);
        p1.add(buttonEqual);

        windowContent.add("Center", p1);

        JPanel p2 = new JPanel();
        GridLayout gl2 = new GridLayout(4, 3);
        p2.setLayout(gl2);

        p2.add(buttonPlus);
        p2.add(buttonMinus);
        p2.add(buttonMultiply);
        p2.add(buttonSqr);
        p2.add(buttonDivide);
        p2.add(buttonSqrt);
        p2.add(buttonLn);
        p2.add(buttonExp);
        p2.add(buttonF);
        p2.add(buttonZnak);
        p2.add(buttonDelete);
        p2.add(buttonClear);


        windowContent.add("East", p2);

        JFrame frame = new JFrame("Modern Calculator");
        frame.setContentPane(windowContent);

        WindowListener wndCloser = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        };
        frame.addWindowListener(wndCloser);

        frame.setSize(600, 300);

        frame.setVisible(true);

        CalculatorEngine calculatorEngine = new CalculatorEngine(this);
        button0.addActionListener(calculatorEngine);
        button1.addActionListener(calculatorEngine);
        button2.addActionListener(calculatorEngine);
        button3.addActionListener(calculatorEngine);
        button4.addActionListener(calculatorEngine);
        button5.addActionListener(calculatorEngine);
        button6.addActionListener(calculatorEngine);
        button7.addActionListener(calculatorEngine);
        button8.addActionListener(calculatorEngine);
        button9.addActionListener(calculatorEngine);
        buttonPoint.addActionListener(calculatorEngine);
        buttonEqual.addActionListener(calculatorEngine);
        buttonPlus.addActionListener(calculatorEngine);
        buttonMinus.addActionListener(calculatorEngine);
        buttonDivide.addActionListener(calculatorEngine);
        buttonMultiply.addActionListener(calculatorEngine);
        buttonClear.addActionListener(calculatorEngine);
        buttonSqrt.addActionListener(calculatorEngine);
        buttonSqr.addActionListener(calculatorEngine);
        buttonDelete.addActionListener(calculatorEngine);
        buttonZnak.addActionListener(calculatorEngine);
        buttonLn.addActionListener(calculatorEngine);
        buttonExp.addActionListener(calculatorEngine);
        buttonF.addActionListener(calculatorEngine);

    }

    public static void main(String[] args) {
        Calculator calc = new Calculator();
        System.out.println("Operation DEL work only with integer numbers(");
    }
}